# adding the transactions
curl --location --request PUT 'http://127.0.0.1:8080/transactionservice/transaction/1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "type": "cloth",
    "amount": 1,
    "parentId": null
}'

curl --location --request PUT 'http://127.0.0.1:8080/transactionservice/transaction/2' \
--header 'Content-Type: application/json' \
--data-raw '{
    "type": "cloth",
    "amount": 3,
    "parentId": 1
}'

curl --location --request PUT 'http://127.0.0.1:8080/transactionservice/transaction/3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "type": "cloth",
    "amount": 5,
    "parentId": 1
}'

curl --location --request PUT 'http://127.0.0.1:8080/transactionservice/transaction/4' \
--header 'Content-Type: application/json' \
--data-raw '{
    "type": "cloth",
    "amount": 7,
    "parentId": 3
}'



curl --location --request PUT 'http://127.0.0.1:8080/transactionservice/transaction/5' \
--header 'Content-Type: application/json' \
--data-raw '{
    "type": "cloth",
    "amount": 11,
    "parentId": 3
}'

# Fetching the transactions 
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/transaction/1'
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/transaction/2'
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/transaction/3'
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/transaction/4'
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/transaction/5'

# validating fetch ids by type
curl --location --request GET 'http://localhost:8080/transactionservice/types/cloth'

# validating sum 
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/sum/1'
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/sum/3'
curl --location --request GET 'http://127.0.0.1:8080/transactionservice/sum/5'
