package com.pocketaces.transactions.repository;

import com.pocketaces.transactions.models.TransactionDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author narendra.vardi
 */
@Repository
public interface TransactionalRepository extends JpaRepository<TransactionDetails, Long> {
    List<TransactionDetails> findAllByType(String type);

    List<TransactionDetails> findAllByParentId(Long id);

    List<TransactionDetails> findAllByParentIdIn(List<Long> ids);

}
