package com.pocketaces.transactions.models;

/**
 * @author narendra.vardi
 */

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Success {
    private String status = "ok";
}
