package com.pocketaces.transactions.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author narendra.vardi
 */
@Data
@Entity
public class TransactionDetails {
    private String type;
    private double amount;
    @Id
    private long id;
    private Long parentId;
}
