package com.pocketaces.transactions.controller;

import com.pocketaces.transactions.models.Success;
import com.pocketaces.transactions.models.TransactionDetails;
import com.pocketaces.transactions.service.TransactionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author narendra.vardi
 */
@RestController
public class TransactionController {
    @Autowired
    private TransactionalService service;

    @PutMapping(path = "/transactionservice/transaction/{id}")
    public Success transactionPut(@PathVariable("id") final Long id, @RequestBody TransactionDetails details) {
        service.save(id, details);
        return new Success();
    }

    @GetMapping(path = "/transactionservice/transaction/{id}")
    public TransactionDetails transactionGet(@PathVariable("id") final Long id) {
        return service.fetch(id);
    }

    @GetMapping(path = "/transactionservice/types/{type}")
    public List<Long> transactionGetByType(@PathVariable("type") final String type) {
        return service.findAllByType(type);
    }

    @GetMapping(path = "/transactionservice/sum/{id}")
    public Double transactionsSum(@PathVariable("id") final Long id) {
        return service.transactionsSum(id);
    }
}
