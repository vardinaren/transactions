package com.pocketaces.transactions.service;

import com.pocketaces.transactions.models.TransactionDetails;
import com.pocketaces.transactions.repository.TransactionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpClientErrorException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author narendra.vardi
 */
@Service
public class TransactionalService {
    @Autowired
    private TransactionalRepository repository;

    public void save(Long id, TransactionDetails details) {
        details.setId(id);
        repository.save(details);
    }

    public TransactionDetails fetch(Long id) {
        return repository.findById(id).orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));
    }

    public List<Long> findAllByType(String type) {
        return repository.findAllByType(type).stream().map(TransactionDetails::getId).collect(Collectors.toList());
    }

    @Transactional
    public Double transactionsSum(Long id) {
        List<TransactionDetails> curr = fetchChildren(id);
        double sum = 0.0;
        while (!ObjectUtils.isEmpty(curr)) {
            sum += curr.stream().map(TransactionDetails::getAmount).reduce(0.0, Double::sum);
            List<Long> ids = curr.stream().map(TransactionDetails::getId).collect(Collectors.toList());
            curr = repository.findAllByParentIdIn(ids);
        }
        return sum;
    }

    List<TransactionDetails> fetchChildren(Long id) {
        return repository.findAllByParentId(id);
    }
}
